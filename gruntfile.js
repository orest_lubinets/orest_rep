module.exports = function(grunt) {

	  grunt.initConfig({
	    pkg: grunt.file.readJSON('package.json'),

	    concat: {

	    	options: {
		      separator: '\n',
		    },

		    dist: {
		      src: ['app/src/app.js', 'app/src/main_component/*.js'],
		      dest: 'app/build/built.js'
		    }
	    },

	    uglify: {

	    	build: {
		        src: 'app/build/built.js',
		        dest: 'app/build/built.js'
		    }
	    },

	    watch: {
		    scripts: {
		        files: ['app/src/app.js', 'app/src/main_component/*.js'],
		        tasks: ['concat', 'uglify'],
		        options: {
		            spawn: false,
		        }
		    } 
		}



	  });

    require('time-grunt')(grunt);

    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-watch');

    grunt.registerTask('default', ['concat', 'uglify']);

};