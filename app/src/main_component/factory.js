
//==========Factories===============

// numPadFactory returns an object with an acces to two arrays of calculator buttons. 
// One is for numbers 1-9. Another one 'Operators' for all of the remaining operators.
(function () {

	angular.module('myApp')
		.factory ('NumPadFactory', numPadFactory)

	function numPadFactory () {

		var numPad = {};
		numPad.numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9];
		numPad.operators = [
			{ name : 'x',   operation : function () {} },
			{ name : '/',   operation : function () {} },
			{ name : '+',   operation : function () {} },
			{ name : '-',   operation : function () {} },
			{ name : '=',   operation : function () {} },
			{ name : '0',   operation : function (param) { 
				if (typeof param.val !== 'string') {
					param.val*=10;
					} else {
						param.val += 0;
						param.val = parseFloat(param.val);
						console.log(typeof param.val) 	
					} } 
			}, //works now
			{ name : '.',   operation : function (param) {param.val = param.val + '.';} },
			{ name : 'C',   operation : function (param) {param.val = 0} },	
			{ name : '+/-',   operation : function () {} },
			{ name : '^',   operation : function () {} }

		];

		numPad.sideOps = numPad.operators.slice(0, 5);
		numPad.bottomOps = numPad.operators.slice(5, 7);
		numPad.topOps = numPad.operators.slice(7);

		return {
			getNumbers : function () {
				return numPad.numbers;
			},
			getOperators : function () {
				return {
					topOps : numPad.topOps,
					bottomOps : numPad.bottomOps,
					sideOps : numPad.sideOps
				}
			}
		}

	}

})();