(function () {


	angular.module('myApp')
		.controller ('CalcController', CalcController)

		//==========Controllers===============

		function CalcController ($scope, NumPadFactory) {

			$scope.visibleState = {val : 0}
			$scope.currentState;
			$scope.numbers = NumPadFactory.getNumbers();
			$scope.operators = NumPadFactory.getOperators(); //returns 3 groups of operators: bottomOps, topOps and sideOps

			$scope.input = function ($index) { 

				var number = $index + 1;
				$scope.visibleState.val = $scope.visibleState.val? $scope.visibleState.val * 10 + number : number;

			};

		}
})();